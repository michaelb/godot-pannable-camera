Godot Pannable Camera
---------------------

Godot Pannable Camera is a simple camera scene useful for RPGs, platformers,
and RTS games.  It has a top-down (or angled) camera that has built in
key-bindings for panning around using WASD keys, rotating, and adjusting FOV /
distance.

It's very useful for rapidly prototyping level editors. It also has the ability
to track another node, such as the player character or a cursor.

Originally made for an upcoming voxel game [I'm working
on](http://michaelb.org/).


## Usage

Very simple, just drop it in the scene, and assign it to be the main
camera!

It's attributes have setters, so if you sync scene changes, anything
you tweak will show up right away.

You can also manually assign the `target` attribute to another
`Vector3`, to manually have the camera focus on a given location.

## Default keys

If you have `keyboard_control` enabled (default), then you get
some basic keyboard control to move around the camera, as follows:

* `WASD` - Pans the camera around the `x` and `z` axis

* `QE` - Rotates the camera (around the `y` axis)

* `RF` - Changes the angle of the camera. Angle defaults to 45&deg;,
  can go up to 90&deg; for straight down, or 0&deg; for ground-level.

* `+-` - Moves toward or away from ground / target (no shift required,
  so `=-` works too)

* `[]` - Adjusts `fovy_trombone` property, effectively changing field
  of view and distance at once, for a "flattening" or "deepening"
  effect, reminiscent of dolly zoom (still WIP)

These default keys are great for rapidly prototyping, or building
editors. `single` only triggers once per key press (slightly less
processing), while the default `smooth` glides along as you hold down
keys.


## Methods

* `start_tracking(target_node)` - Sets the camera to focus on the given node.
  When this node moves, the camera will move to (assuming `get_translation`
  gives the nodes location in 3D space).

* `stop_tracking()` - Stops the camera from tracking a given node.

## Extra properties

It inherits Camera, so all the old proprieties still apply, in addition to some new ones:

```
# rotation around the y axis
export(int, 0, 360) var rotation = 0

# angle of camera w.r.t. y axis
export(int, 0, 90) var angle = 45

# fovy_trombone is field of view, but attempts to keep the distance the same-ish
export(int, 0, 90) var fovy_trombone = 10

# distance from target
export(int, 0, 500) var distance = 20

# A Vector3 specifying where the camera is pointed
export(Vector3) var target = Vector3(0, 0, 0)

# Enable or disable keyboard control
export(bool) var keyboard_control_enabled = true
```

## Signals

`signal moved` - only one signal, which is emitted when it is moved or
adjusted by any means (incl. keyboard)
